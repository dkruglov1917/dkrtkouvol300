﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerManager : MonoBehaviour
{
    public TMPro.TMP_Text text;
    public bool isTowerLimit;
    public static bool stop;

    private void Update()
    {
        text.text = "Tower: " + transform.childCount;

        if (transform.childCount > 100 && !stop)
        {
            stop = true;
            isTowerLimit = true;
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).GetComponent<Tower>().courInt = 0;
                transform.GetChild(i).GetComponent<Tower>().StartCoroutine("Initialize");
            }
        }
    }
}
