﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shell : MonoBehaviour
{
    public GameObject towersObj;
    public GameObject targetPrefab;

    public GameObject originTower;

    public float speed;
    public float targetDistance;

    private void Start()
    {
        towersObj = GameObject.Find("Towers");
    }

    private void Update()
    {
        if (originTower != null && Vector3.Distance(transform.position, originTower.transform.position) >= targetDistance)
        {
            if (!TowerManager.stop)
            {
                GameObject cloneTower = Instantiate(originTower, transform.position, Quaternion.identity);
                cloneTower.transform.parent = towersObj.transform;
            }
            Destroy(gameObject);
        }
    }

    private void FixedUpdate()
    {
        transform.Translate(Vector3.up * Time.deltaTime);
    }

    public void SetTarget(GameObject _originTower)
    {
        targetDistance = Random.Range(1, 4);
        originTower = _originTower;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Tower" && other.gameObject != originTower)
        {
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }
}
