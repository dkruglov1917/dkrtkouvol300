﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    public GameObject shellPrefab;
    private GameObject shellsObj;
    public SpriteRenderer spriteRenderer;

    public float towerRotation = 0f;
    public int upperRotLimit = 45;
    public int lowerRotLimit = 15;
    public int courInt = 6;

    private float nextActionTime = 0.0f;
    public float period = 5f;

    public int numOfShots;

    private bool ready;

    private void Start()
    {
        spriteRenderer = transform.GetChild(0).GetComponent<SpriteRenderer>();
        shellsObj = GameObject.Find("Shells");
        StartCoroutine(Initialize());
    }

    void Update()
    {
        ManageColor();

        if (ready && numOfShots <= 12)
        {
            if (Time.time > nextActionTime)
            {
                nextActionTime += period;
                towerRotation += Random.Range(lowerRotLimit, upperRotLimit);
                transform.Rotate(new Vector3(0, 0, towerRotation));
                towerRotation = 0;

                Shoot();
            }
        }

        if (numOfShots > 12 && !TowerManager.stop)
        {
            ready = false;
        }
    }

    private void ManageColor()
    {
        if (!ready)
        {
            spriteRenderer.color = Color.white;
        }
        else if (ready && numOfShots <= 12)
        {
            spriteRenderer.color = Color.red;
        }
    }

    private void Shoot()
    {
        GameObject cloneShell = Instantiate(shellPrefab, transform, true);
        cloneShell.transform.position = transform.position;
        cloneShell.transform.Rotate(transform.eulerAngles);
        cloneShell.GetComponent<Shell>().SetTarget(gameObject);
        cloneShell.transform.parent = shellsObj.transform;


        numOfShots += 1;
    }

    public IEnumerator Initialize()
    {
        yield return new WaitForSeconds(courInt);
        ready = true;
        nextActionTime = Time.time;
        numOfShots = 0;
    }
}
