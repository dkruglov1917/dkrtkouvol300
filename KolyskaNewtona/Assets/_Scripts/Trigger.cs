using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger : MonoBehaviour
{
    public Rigidbody2D mainCircle, affectedCircle;
    public bool isRight;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (isRight)
            affectedCircle.AddForce(transform.right * mainCircle.velocity * 750, ForceMode2D.Impulse);
        else
            affectedCircle.AddForce(-transform.right * mainCircle.velocity * 750, ForceMode2D.Impulse);

    }
}
