1)Przepraszam, nie udało mi się dobrze zrobić pierwszego projektu.
2)Wydaje mi się, że w tym zadaniu w Object Pool nie ma sensu, jeśli Destroy jest poprawnie używany, ponieważ nie powinno być wycieków pamięci i nieoczekiwanych obciążeń.

UPD:1)Źle zrozumiałem pierwsze zadanie i spędziłem dużo czasu na jego rozwiązaniu. Początkowo myślałem, że potrzebne jest rozwiązanie oparte wyłącznie na fizycznych możliwościach silnika (przenoszenie pędu przez każdą kulkę), ale doszedłem do wniosku, że nie jest to możliwe w Unity.
Na koniec postanowiłem zaimplementować to po prostu przez hack w postaci 3 nieruchomych kulek i przekazując pęd bezpośrednio do przeciwnej kulki, ale już nie miałem czasu, aby w pełni to zrealizować.
2)W drugim zadaniu sensowne jest wdrożenie Object Pool w przypadku planowanego rozszerzenia projektu,
ponieważ w przypadku korzystania z większej liczby liczby obiektów utrata pamięci może być zauważalna.